===========
DRAG Editor
===========

Ferramenta para facer rapidamente edicións habituais de no módulo
``rag/gl/correcto.dic``.

Requisitos
==========

A ferramenta require installar os paquetes de Python listados en
``requirements.txt``.

Recoméndase facelo nun ambiente virtual de Python.


Uso
===

Cumpridos os requisitos, execute::

    ./edit.py --help
