#!/usr/bin/env python3

from pathlib import Path
from os import remove
import re
from shutil import move
from tempfile import TemporaryDirectory

from click import argument, command, ClickException
from icu import Collator, Locale


@command()
@argument('parts', nargs=-1, metavar='entry')
def main(parts):
    """Adds an entry to the right line of ‘rag/gl/correcto.dic’.

    Example:

        edit.py cuadrinxentésimo/10,15 po:adxectivo
    """
    build_dir = TemporaryDirectory()
    tmp_filepath = Path(build_dir.name) / 'a'
    filepath = Path(__file__).parent.parent.parent / 'src/rag/gl/correcto.dic'
    collator = Collator.createInstance(Locale('gl_ES.UTF-8'))
    in_match, written = False, False
    entry = ' '.join(parts) + '\n'
    match = re.match(r'^[^ ]+ (.*)\n', entry)
    pattern = re.compile(rf'^([^ ]+) {match.group(1)}\n')

    with tmp_filepath.open('w') as output_file:
        with filepath.open('r') as input_file:
            for line in input_file:
                if written:
                    output_file.write(line)
                elif in_match:
                    if (not pattern.match(line) or
                            collator.compare(entry, line) < 0):
                        output_file.write(entry)
                        written = True
                    output_file.write(line)
                elif pattern.match(line):
                    in_match = True
                    if collator.compare(entry, line) < 0:
                        output_file.write(entry)
                        written = True
                    output_file.write(line)
                else:
                    output_file.write(line)

    remove(filepath)
    move(tmp_filepath, filepath)


if __name__ == '__main__':
    main()
