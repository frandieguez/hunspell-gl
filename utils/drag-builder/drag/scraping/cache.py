from scrapy.extensions.httpcache import DummyPolicy


class Policy(DummyPolicy):

    def is_cached_response_fresh(self, cachedresponse, request):
        return request.meta.get('refresh_cache') is not True

    def is_cached_response_valid(self, cachedresponse, response, request):
        return request.meta.get('refresh_cache') is not True