from scrapy import Spider


class BaseSpider(Spider):
    custom_settings = {
        # Be gentle.
        'AUTOTHROTTLE_ENABLED': True,

        # Disable unused extensions, downloader middlewares and spider
        # middlewares.
        'EXTENSIONS': {
            'scrapy.extensions.telnet.TelnetConsole': None,
            'scrapy.extensions.memusage.MemoryUsage': None,
        },
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware': None,
            'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware': None,
            'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware': None,
            'scrapy.downloadermiddlewares.cookies.CookiesMiddleware': None,
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
            'scrapy.downloadermiddlewares.stats.DownloaderStats': None,
        },
        'SPIDER_MIDDLEWARES': {
            'scrapy.spidermiddlewares.offsite.OffsiteMiddleware': None,
            'scrapy.spidermiddlewares.depth.DepthMiddleware': None,
        },

        # Cache webpages indefinitely.
        'HTTPCACHE_ENABLED': True,
        'HTTPCACHE_EXPIRATION_SECS': 0,
        'HTTPCACHE_POLICY': 'drag.scraping.cache.Policy',

        # Reduce noise.
        'LOG_LEVEL': 'INFO',
    }
